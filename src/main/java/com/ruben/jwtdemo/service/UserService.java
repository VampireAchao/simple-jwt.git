package com.ruben.jwtdemo.service;

import com.baomidou.mybatisplus.extension.api.R;
import com.ruben.jwtdemo.pojo.User;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户业务层
 *
 * @author <achao1441470436@gmail.com>
 * @since 2021/6/6 0006 22:10
 */
public interface UserService {

    String getToken(HttpServletRequest request);

    User verifyToken(String token);

    String getUsernameByToken(String token);

    User getUserByUsername(String username);

    User validateUser(User param);

    String createToken(User user, int expire);

    R refreshToken(String refreshToken);

    boolean logout(String token);

    String register(User user);
}
