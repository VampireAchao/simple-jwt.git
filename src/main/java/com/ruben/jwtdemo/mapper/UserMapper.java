package com.ruben.jwtdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruben.jwtdemo.pojo.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户Mapper
 *
 * @author <achao1441470436@gmail.com>
 * @since 2021/6/6 0006 22:31
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
