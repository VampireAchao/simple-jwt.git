package com.ruben.jwtdemo.pojo;

import lombok.*;

import java.io.Serializable;

/**
 * 用户
 *
 * @author <achao1441470436@gmail.com>
 * @since 2021/6/6 0006 22:11
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private static final long serialVersionUID = 2436846831898074611L;
    private Integer id;
    private String username;
    private String password;
}
